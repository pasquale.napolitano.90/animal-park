public with sharing class AnimalCarouselController {
	@AuraEnabled(cacheable=true)
	public static List<String> getPictures(Id parkId) {
		system.debug(parkId);

		List<Park_Sighting__c> sighs = [
			SELECT Animal__r.Id, Animal__r.Image_Url__c
			FROM Park_Sighting__c
			WHERE Park__c = :parkId
			
		];

		if (sighs.isEmpty()) {
			return null;
		}

		List<String> urls = new List<String>();

		for (Park_Sighting__c ps : sighs) {
			urls.add(ps.Animal__r.Image_Url__c);
		}

		system.debug(urls);

		return urls;
	}
}
