/* eslint-disable no-console */
import { LightningElement, wire, track, api } from 'lwc';
import getPictures from '@salesforce/apex/AnimalCarouselController.getPictures';


export default class AnimalsCarousel extends LightningElement {
	@api recordId;
	@track urls;

	@wire(getPictures, { parkId: '$recordId' })
	wiredUrls(urls){
		console.log(urls);
		this.urls = urls;
	}
	
}